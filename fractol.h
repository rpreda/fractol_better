#ifndef FRACTOL_H
# define FRACTOL_H
# include "libft.h"
# include "mlx.h"
# include <math.h>
# include <stdlib.h>
# include <pthread.h>
# define WIN_SIZE 600
# define INITIAL_ITERATIONS 100
# define MAX_ITERATIONS 500
# define CONSTANT_RE -0.8
# define CONSTANT_IM 0.176
# define INITIAL_ZOOM 100
# define ZOOM_ALTER 1.6
# define COLOR_MULT 16183215
# define SCROLL_UP 4
# define SCROLL_DOWN 5
# define X_OFFSET_STEP 20
# define Y_OFFSET_STEP 20

typedef struct	s_complex
{
	long double	re;
	long double	im;
}				t_complex;

typedef struct	s_env
{
	void 		*mlx;
	void 		*win;
	int			endian;
	int			bpp;
	int			ln_size;
	void		*image;
	char		*img;
	int			max_iterations;
	t_complex	constant;
	long double zoom;
	long double zoom_alter;
	long double ox;
	long double oy;
	void 		*(*fractal_thread)(void *);
	int			color_table[MAX_ITERATIONS];
	int			color_mult;
	int			**thread_starts;
	pthread_t	threads[4];
}				t_env;

typedef struct	s_thread_data
{
	t_env		*env;
	int			*start_stop;
	int			x;
}				t_thread_data;

typedef struct	s_interval
{
	float start;
	float end;
}				t_interval;
#endif