/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 16:00:01 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 16:00:02 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*cpy;
	size_t	strlen;
	int		i;

	i = 0;
	strlen = ft_strlen(s);
	cpy = (char*)malloc(sizeof(char) * (len + 1));
	if (!cpy)
		return (0);
	while (len--)
	{
		cpy[i++] = s[start++];
	}
	cpy[i] = '\0';
	return (cpy);
}
