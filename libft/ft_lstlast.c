/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstlast.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:50:55 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:50:57 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstlast(t_list **alst, t_list *last)
{
	t_list	*list;

	if (alst != NULL)
	{
		if (*alst == NULL)
			*alst = last;
		else
		{
			list = *alst;
			while (list->next != NULL)
				list = list->next;
			list->next = last;
		}
	}
}
