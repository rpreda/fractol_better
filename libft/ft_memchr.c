/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:51:22 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:51:24 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t len)
{
	unsigned char *temp;

	temp = (unsigned char*)s;
	while (len--)
	{
		if (*temp++ == (unsigned char)c)
			return (void *)(temp - 1);
	}
	return (0);
}
