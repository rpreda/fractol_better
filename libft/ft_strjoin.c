/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:57:31 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:57:35 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	len1;
	size_t	len2;
	char	*cp;
	char	*cpy;
	int		i;

	i = 0;
	len1 = ft_strlen(s1);
	len2 = ft_strlen(s2);
	cp = (char*)malloc(sizeof(char) * (len1 + len2 + 1));
	if (!cp)
		return (0);
	cpy = cp;
	while (*s1)
	{
		*cpy++ = *s1++;
	}
	while (*s2)
	{
		*cpy++ = *s2++;
	}
	*cpy = 0;
	return (cp);
}
