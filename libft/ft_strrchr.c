/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:59:41 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:59:42 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char *str;
	char ch;
	char *d;

	d = 0;
	ch = (char)c;
	str = (char*)s;
	while (*str)
	{
		if (*str == ch)
			d = str;
		str++;
	}
	if (!c && !*str)
		return (str);
	if (d)
		return (d);
	return (0);
}
