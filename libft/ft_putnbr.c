/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:54:09 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:54:11 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_putnbr_rec(long nb)
{
	if (nb < 0)
	{
		nb *= (-1);
		ft_putchar('-');
	}
	if (nb > 0)
	{
		ft_putnbr_rec(nb / 10);
		ft_putchar((nb % 10) + '0');
	}
}

void			ft_putnbr(long nb)
{
	if (nb == 0)
		ft_putchar('0');
	else
		ft_putnbr_rec(nb);
}
