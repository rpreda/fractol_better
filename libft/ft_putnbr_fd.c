/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:54:03 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:54:05 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		ft_putnbr_fd_rec(long nb, int fd)
{
	if (nb < 0)
	{
		nb *= (-1);
		ft_putchar_fd('-', fd);
	}
	if (nb > 0)
	{
		ft_putnbr_fd_rec(nb / 10, fd);
		ft_putchar_fd(((nb % 10) + '0'), fd);
	}
}

void			ft_putnbr_fd(long nb, int fd)
{
	if (nb == 0)
		ft_putchar_fd('0', fd);
	else
		ft_putnbr_fd_rec(nb, fd);
}
