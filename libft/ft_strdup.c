/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:56:37 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:56:40 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	size_t		len;
	char		*s2;
	const char	*src;
	char		*new;

	src = s1;
	len = ft_strlen(s1);
	s2 = (char*)malloc((len + 1) * sizeof(char));
	if (!s2)
		return (0);
	new = s2;
	while (len--)
		*new++ = *src++;
	*new = 0;
	return (s2);
}
