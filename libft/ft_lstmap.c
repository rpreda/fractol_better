/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:51:02 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:51:04 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new;
	t_list	*compot;

	if (!lst)
		return (NULL);
	compot = f(lst);
	new = compot;
	while (lst->next)
	{
		lst = lst->next;
		if (!(compot->next = f(lst)))
		{
			free(compot->next);
			return (NULL);
		}
		compot = compot->next;
	}
	return (new);
}
