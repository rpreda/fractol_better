/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:59:09 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:59:11 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t len;

	len = ft_strlen(little);
	if (!len)
		return ((char *)(big));
	while (*big)
	{
		if (!ft_memcmp(big, little, len))
			return ((char*)(big));
		big++;
	}
	return (0);
}
