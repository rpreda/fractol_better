/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:58:12 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:58:14 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*s2;
	char	*s2cpy;
	int		i;

	i = 0;
	s2 = (char*)ft_memalloc(ft_strlen(s) + 1);
	if (!s2)
		return (0);
	s2cpy = s2;
	while (*s)
	{
		*s2cpy = f(i++, *s);
		s++;
		s2cpy++;
	}
	return (s2);
}
