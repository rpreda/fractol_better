/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:58:56 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:59:00 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t len2;

	if (!*little)
		return (char*)(big);
	len2 = ft_strlen(little);
	while (*big && len-- >= len2)
	{
		if (!ft_memcmp(big, little, len2))
			return ((char*)(big));
		big++;
	}
	return (0);
}
