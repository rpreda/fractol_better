/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpreda <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 15:52:56 by rpreda            #+#    #+#             */
/*   Updated: 2018/02/13 15:52:58 by rpreda           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*d;
	char		*s;
	size_t		i;

	i = 0;
	d = (char *)dst;
	s = (char *)src;
	if (d <= s)
		ft_memcpy(dst, src, len);
	else
		while (len--)
			d[len] = s[len];
	return (d);
}
