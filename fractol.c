#include "fractol.h"
void		fractal_draw(t_env *env);

char	blowing_up(t_complex num)
{
	if ((long double)(num.re * num.re + num.im * num.im) > 4)
		return (1);
	return (0);
}

float	map_interval(float x, t_interval old_range, t_interval new_range)
{
	float output;

	output = new_range.start + ((new_range.end - new_range.start) /
		(old_range.end - old_range.start)) * (x - old_range.start);
	return (output);
}

void	put_pixel(t_env *env, int x, int y, int col)
{
	int offset;

	offset = x * 4;
	offset += y * env->ln_size;
	*(int *)(env->img + offset) = col;
}

void		clear_image(t_env *env)
{
	int i;

	i = 0;
	while (i < env->ln_size * WIN_SIZE)
		env->img[i++] = 0;
}

int			key_hook(int keycode, void *ev)
{
	t_env *env;

	env = (t_env *)ev;
	if (keycode == 53)
		exit(0);
	if (keycode == 123)
		env->ox -= X_OFFSET_STEP;
	if (keycode == 124)
		env->ox += X_OFFSET_STEP;
	if (keycode == 126)
		env->oy -= Y_OFFSET_STEP;
	if (keycode == 125)
		env->oy += Y_OFFSET_STEP;
	fractal_draw(env);
	return (0);
}

int			cursor_hook(int x, int y, void *ev)
{
	t_env		*env;
	t_interval	screen;
	t_interval	coords_range;

	(void)y;
	screen.start = 0;
	screen.end = WIN_SIZE;
	coords_range.start = 0;
	coords_range.end = 3.14;
	env = (t_env *)ev;
	env->constant.re = map_interval(x, screen, coords_range);
	fractal_draw(env);
	return (0);
}

int		scroll_hook(int buttoncode, int x, int y, void *ev)
{
	t_env *env;

	env = (t_env *)ev;
	if (buttoncode == SCROLL_UP)
	{
		if (env->max_iterations >= 98)
			env->max_iterations -= 4;
		env->ox = x - (x - env->ox) / env->zoom_alter;
		env->oy = y - (y - env->oy) / env->zoom_alter;
		if (env->zoom >= 180)
			env->zoom /= env->zoom_alter;
		fractal_draw(env);
	}
	if (buttoncode == SCROLL_DOWN)
	{
		if (env->max_iterations < 500)
			env->max_iterations += 4;
		env->ox = x - (x - env->ox) * env->zoom_alter;
		env->oy = y - (y - env->oy) * env->zoom_alter;
		env->zoom *= env->zoom_alter;
		fractal_draw(env);
	}
	return (0);
}

int			expose_hook(void *env)
{
	t_env *ev;

	ev = (t_env *)env;
	mlx_put_image_to_window(ev->mlx, ev->win, ev->image, 0, 0);
	return (0);
}

t_complex	complex_mult(t_complex a, t_complex b)
{
	t_complex ret_val;

	ret_val.re = a.re * b.re - a.im * b.im;
	ret_val.im = a.re * b.im + b.re * a.im;
	return (ret_val);
}

t_complex	absolutify(t_complex a)
{
	if (a.re < 0)
		a.re = -a.re;
	if (a.im < 0)
		a.im = -a.im;
	return (a);
}

t_complex	complex_add(t_complex a, t_complex b)
{
	t_complex	ret_val;

	ret_val.re = a.re + b.re;
	ret_val.im = a.im + b.im;
	return (ret_val);
}

void		pre_compute_color(t_env *env)
{
	int i;

	i = 0;
	while (i < MAX_ITERATIONS)
	{
		env->color_table[i] = env->color_mult * i;
		env->color_table[i] &= 0x00FFFFFF;
		i++;
	}
}

void		init_thread_indexes(t_env *env)
{
	int i;

	i = 0;
	env->thread_starts = (int **)malloc(sizeof(int *) * 4);
	while (i < 4)
		env->thread_starts[i++] = (int *)malloc(sizeof(int) * 2);
	env->thread_starts[0][0] = 0;
	env->thread_starts[0][1] = 150;
	env->thread_starts[1][0] = 150;
	env->thread_starts[1][1] = 300;
	env->thread_starts[2][0] = 300;
	env->thread_starts[2][1] = 450;
	env->thread_starts[3][0] = 450;
	env->thread_starts[3][1] = 600;
}

void		init_values(t_env *env)
{
	env->zoom = INITIAL_ZOOM;
	env->zoom_alter = ZOOM_ALTER;
	env->constant.re = CONSTANT_RE;
	env->constant.im = CONSTANT_IM;
	env->max_iterations = INITIAL_ITERATIONS;
	env->ox = WIN_SIZE / 2;
	env->oy = WIN_SIZE / 2;
	env->color_mult = COLOR_MULT;
	init_thread_indexes(env);
	pre_compute_color(env);
}

t_thread_data		*pack_thread_data(t_env *env, int *start_stop)
{
	t_thread_data	*ret_val;

	ret_val = malloc(sizeof(t_thread_data));
	ret_val->env = env;
	ret_val->start_stop = start_stop;
	ret_val->x = start_stop[0];
	return (ret_val);
}

void		*julia_thread(void *param)
{
	t_thread_data	*data;
	int				iter;
	t_complex		z;
	int				y;

	data = (t_thread_data *)param;
	while (data->x < data->start_stop[1])
	{
		y = 0;
		while (y < WIN_SIZE)
		{
			z.re = (data->x - data->env->ox) / data->env->zoom;
			z.im = (y - data->env->oy) / data->env->zoom;
			iter = -1;
			while (!blowing_up(z) && ++iter < data->env->max_iterations)
			{
				z = complex_mult(z, z);
				z = complex_add(z, data->env->constant);
			}
			put_pixel(data->env, data->x, y, data->env->color_table[iter]);
			y++;
		}
		data->x++;
	}
	return (0);
}

void		init_val_sh_man(t_complex *z, t_complex *c,
	t_thread_data *data, int y)
{
	z->re = 0;
	z->im = 0;
	c->re = (data->x - data->env->ox) / data->env->zoom;
	c->im = (y - data->env->oy) / data->env->zoom;
}

void		*mandel_thread(void *param)
{
	t_thread_data	*data;
	int				iter;
	t_complex		c;
	t_complex		z;
	int				y;

	data = (t_thread_data *)param;
	while (data->x < data->start_stop[1])
	{
		y = 0;
		while (y < WIN_SIZE)
		{
			init_val_sh_man(&z, &c, data, y);
			iter = 0;
			while (!blowing_up(z) && iter < data->env->max_iterations)
			{
				z = complex_add(complex_mult(z, z), c);
				iter++;
			}
			put_pixel(data->env, data->x, y, data->env->color_table[iter]);
			y++;
		}
		data->x++;
	}
	return (0);
}

void		*ship_thread(void *param)
{
	t_thread_data	*data;
	int				iter;
	t_complex		z;
	t_complex		z2;
	int				y;

	data = (t_thread_data *)param;
	while (data->x < data->start_stop[1])
	{
		y = 0;
		while (y < WIN_SIZE)
		{
			init_val_sh_man(&z2, &z, data, y);
			iter = 0;
			while (!blowing_up(z2) && iter < data->env->max_iterations)
			{
				z2 = complex_add(absolutify(complex_mult(z2, z2)), z);
				iter++;
			}
			put_pixel(data->env, data->x, y, data->env->color_table[iter]);
			y++;
		}
		data->x++;
	}
	return (0);
}

void		fractal_draw(t_env *env)
{
	int i;

	i = -1;
	clear_image(env);
	while (++i < 4)
		pthread_create(&env->threads[i], NULL, env->fractal_thread,
			pack_thread_data(env, env->thread_starts[i]));
	pthread_join(env->threads[0], NULL);
	pthread_join(env->threads[1], NULL);
	pthread_join(env->threads[2], NULL);
	pthread_join(env->threads[3], NULL);
	mlx_put_image_to_window(env->mlx, env->win, env->image, 0, 0);
}

void		select_fractal(t_env *env, char *arg)
{
	if (*arg == 'J' || *arg == 'j')
		env->fractal_thread = julia_thread;
	else if (*arg == 'M' || *arg == 'm')
		env->fractal_thread = mandel_thread;
	else if (*arg == 'S' || *arg == 's')
		env->fractal_thread = ship_thread;
	else
	{
		ft_putendl("Usage: ./fractol J-juilia M-mandelbrot S-burning ship");
		exit(-1);
	}
}

int			main(int ac, char **av)
{
	t_env env;

	if (ac != 2 || av[1][1])
	{
		ft_putendl("Usage: ./fractol J-juilia M-mandelbrot S-burning ship");
		return (-2);
	}
	env.mlx = mlx_init();
	env.win = mlx_new_window(env.mlx, WIN_SIZE, WIN_SIZE, "Fract'ol");
	mlx_key_hook(env.win, key_hook, &env);
	mlx_expose_hook(env.win, expose_hook, &env);
	mlx_hook(env.win, 4, (1L << 2), scroll_hook, &env);
	mlx_hook(env.win, 6, (1L << 6), cursor_hook, &env);
	env.image = mlx_new_image(env.mlx, WIN_SIZE, WIN_SIZE);
	env.img = mlx_get_data_addr(env.image, &env.bpp, &env.ln_size, &env.endian);
	select_fractal(&env, av[1]);
	init_values(&env);
	fractal_draw(&env);
	mlx_loop(env.mlx);
	return (0);
}
